#!/bin/bash

echo "Tidy up..."
EXISTING=$(docker ps -a | grep kcc:latest | grep Exited | cut -f1 -d ' ')
if [ ! "$EXISTING" == "" ]; then
	docker rm $EXISTING > /dev/null
fi

echo "Building..."
docker build -q -t kcc:latest . > /dev/null

CONTAINER=$(docker ps -q --filter "name=kcc")
if [[ ! "$CONTAINER" == "" ]]; then
	echo "Killing existing running container ($CONTAINER)..."
	docker kill $CONTAINER > /dev/null
	docker rm $CONTAINER > /dev/null
fi

docker run -d -p 8080:80 --name kcc kcc:latest > /dev/null
CONTAINER=$(docker ps -q --filter "name=kcc")
if [[ ! "$CONTAINER" == "" ]]; then
	echo "Running on http://localhost:8080"
else
	echo "Failed"
fi




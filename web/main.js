function init() {
	generate_cages();
}

var cages = [];

function select_menu_item(x) {
    const item  = x.innerText;
    const p = x.parentElement;
    const kids = p.children;
    for (var i=0; i<kids.length; i++) {
        kids[i].classList.remove("selected");
    }

	// Hide all pages
    const pages = document.getElementsByClassName("page_tab");
    for (var i=0; i<pages.length; i++) {
        pages[i].classList.remove("active");
    }
    
	// Make the chosen page visible
    x.classList.add("selected");
    const pagename = item+"_page";
	console.log("Pagename",pagename);
    const page = document.getElementById(pagename);
    page.classList.add("active");

}

function generate_cages() {

	// Gernerate a result set of ALL possible combinations
	var result = [];
	var f = function(prefix, chars) {
		for (var i = 0; i < chars.length; i++) {
			result.push(prefix + chars[i]);
			f(prefix + chars[i], chars.slice(i + 1));
		}
	}
  	f('', "123456789");
	console.log(result);

	// Create a more structuresd array containing the value, size and sum
	for (var i=0; i< result.length; i++) {
		var array = result[i];
		var size =  array.length;
		var sum  =  sumOf(array);
		var stanza = {"sum":sum, "size":size, "values":array};
		cages.push(stanza);
	}
}

function sumOf(a) {
	var sum = 0;
	for (var i=0; i<a.length; i++) {
		sum += parseInt(a[i]);
	}
	return sum;
}

function  calc() {
	const size    = document.getElementById("i_size").value;
	const sum     = document.getElementById("i_sum").value;
	const include = document.getElementById("i_include").value;
	const exclude = document.getElementById("i_exclude").value;
	var results_1 = [];
	var results_2 = [];
	var results_3 = [];
	var results_4 = [];

	// Only match if size is specified
	if (size != 0) {
		for (var i=0; i< cages.length; i++) {
			const c = cages[i];
			if (c.size == size) {
				results_1.push(c);
			}
		}
	} else {
		results_1 = cages;
	}

	// Only apply if sum is specified
	if (sum != 0) {
		for (var i=0; i< results_1.length; i++) {
			const c = results_1[i];
			if (c.sum == sum) {
				results_2.push(c);
			}
		}
	} else {
		results_2 = results_1;
	}

        // Only apply if inclusions are specified
        if (include != 0) {
		const inc_array = include.split(',');
                for (var i=0; i< results_2.length; i++) {
                        const c = results_2[i];
                        if (string_includes(c.values, inc_array)) {
                                results_3.push(c);
                        }
                }
        } else {
                results_3 = results_2;
        }

        // Only apply if exclusions are specified
        if (exclude != 0) {
		const excl_array = exclude.split(',');
                for (var i=0; i< results_3.length; i++) {
                        const c = results_3[i];
                        if (string_excludes(c.values, excl_array)) {
                        //if (!c.values.includes(exclude)) {
                                results_4.push(c);
                        }
                }
        } else {
                results_4 = results_3;
        }

	var res = document.getElementById("results_out");
	delete_results("results_out");
	if (results_4.length == 0) {
			const msg = document.getElementById("results_msg");
			msg.innerText = "No results that meet those restrictions";
	} else if (results_4.length > 20) {
			const msg = document.getElementById("results_msg");
			msg.innerText = "Found too many results to be useful (>20)- try restricting the parameters";		
	} else {
		const msg = document.getElementById("results_msg");
		msg.innerText = "";			
		for (var i=0; i< results_4.length; i++) {
			const r = document.createElement("div");
			r.innerHTML = results_4[i].values;
			r.classList.add("result");
			res.appendChild(r);
		}
	}

}

function string_includes(str, arr) {
	for (let i = 0; i < arr.length; i++) {
		console.log(str.indexOf(arr[i]));
		if (str.indexOf(arr[i]) === -1) {
			return false;
		}
	}
	return true;
}

function string_excludes(str, arr) {
        for (let i = 0; i < arr.length; i++) {
                console.log(str.indexOf(arr[i]));
                if (str.indexOf(arr[i]) != -1) {
                        return false;
                }
        }
        return true;
}


function removeAllChildren(element) {
	while (element.firstChild) {
		if (element.firstChild.classList.include("result")) {
			element.removeChild(element.firstChild);
		}
 	}
}

function delete_results(parentId) {
	const parent = document.getElementById(parentId);
	const results = parent.getElementsByClassName("result");
	while (results.length > 0) {
	  results[0].parentNode.removeChild(results[0]);
	}
  }


function clear_fields() {
	var size    = document.getElementById("i_size");
	var sum     = document.getElementById("i_sum");
	var include = document.getElementById("i_include");
	var exclude = document.getElementById("i_exclude");
	size.value = "";
	sum.value = "";
	include.value = "";
	exclude.value = "";
}

variable "AWS_ACCESS_ID" {
  type = string
  default = ""
}

variable "AWS_SECRET_ACCESS_KEY" {
  type = string
  default = ""
}

provider "aws" {
  region = "ap-southeast-2"
  access_key=var.AWS_ACCESS_ID
  secret_key=var.AWS_SECRET_ACCESS_KEY
}

resource "aws_s3_bucket" "kcc_bucket" {
  bucket = "cloudy76-kcc-www"
  acl    = "private"
}

locals {
  files_to_upload = fileset(path.module, "web/*")
}

resource "aws_s3_object" "uploaded_objects" {
  for_each = local.files_to_upload

  bucket = "cloudy76-kcc-www"
  key    = "${each.value}"
  source = "${each.value}"
  acl    = "private"
}

resource "aws_cloudfront_distribution" "kcc_distribution" {
  enabled             = true
  comment             = "kcc.cloudy76.com"
  default_root_object = "index.html"

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD", "OPTIONS"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "cloudy76-kcc-www"
    forwarded_values {
      query_string = false
      cookies {
        forward = "none"
      }
    }
    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    cloudfront_default_certificate = true
  }

  origin {
    domain_name = "cloudy76-kcc-www.s3.amazonaws.com"
    origin_id   = "cloudy76-kcc-www"
    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.example_identity.cloudfront_access_identity_path
    }
  }

  resource "aws_cloudfront_origin_access_identity" "example_identity" {
    comment = "Example CloudFront Origin Access Identity"
  }
}

resource "aws_s3_bucket_policy" "example_policy" {
  bucket = "cloudy76-kcc-www" 

  policy = jsonencode({
    "Version" : "2012-10-17",
    "Id"      : "CloudFrontOriginAccessIdentity",
    "Statement" : [
      {
        "Sid"       : "Grant CloudFront Origin Access Identity access to the bucket",
        "Effect"    : "Allow",
        "Principal" : {
          "CanonicalUser" : aws_cloudfront_origin_access_identity.example_identity.s3_canonical_user_id
        },
        "Action"    : "s3:GetObject",
        "Resource"  : "arn:aws:s3:::cloudy76-kcc-www/*" 
      }
    ]
  })
}

output "kcc_bucket" {
  value = aws_s3_bucket.kcc_bucket.id
}

